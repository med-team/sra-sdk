Source: sra-sdk
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Charles Plessy <plessy@debian.org>,
           Andreas Tille <tille@debian.org>,
           Aaron M. Ucko <ucko@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               cmake,
               d-shlibs,
               dh-python,
               javahelper,
	       libabsl-dev,
               libncbi-vdb-dev (>= 3.0.9+dfsg~),
               libfuse-dev,
               libhdf5-dev,
               libmagic-dev,
               libre2-dev,
               libxml2-dev,
               libzstd-dev,
               liblzma-dev,
               libsam-extract-dev,
               maven-repo-helper,
               python3-all,
               default-jdk,
               flex,
               bison
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/sra-sdk
Vcs-Git: https://salsa.debian.org/med-team/sra-sdk.git
Homepage: https://github.com/ncbi/sra-tools/
Rules-Requires-Root: no

Package: sra-toolkit
Architecture: amd64 arm64
Depends: ${shlibs:Depends},
         ${misc:Depends}
Recommends: med-config
Description: utilities for the NCBI Sequence Read Archive
 Tools for reading the SRA archive, generally by converting individual runs
 into some commonly used format such as fastq.
 .
 The textual dumpers "sra-dump" and "vdb-dump" are provided in this
 release as an aid in visual inspection. It is likely that their
 actual output formatting will be changed in the near future to a
 stricter, more formalized representation[s]. PLEASE DO NOT RELY UPON
 THE OUTPUT FORMAT SEEN IN THIS RELEASE.
 .
 Other tools distributed in this package are:
  abi-dump, abi-load
  align-info
  bam-load
  cache-mgr
  cg-load
  copycat
  fasterq-dump
  fastq-dump, fastq-load
  helicos-load
  illumina-dump, illumina-load
  kar
  kdbmeta
  latf-load
  pacbio-load
  prefetch
  rcexplain
  remote-fuser
  sff-dump, sff-load
  sra-pileup, sra-sort, sra-stat, srapath
  srf-load
  test-sra
  vdb-config, vdb-copy, vdb-decrypt, vdb-encrypt, vdb-get, vdb-lock,
  vdb-passwd, vdb-unlock, vdb-validate
 .
 The "help" information will be improved in near future releases, and
 the tool options will become standardized across the set. More documentation
 will also be provided documentation on the NCBI web site.
 .
 Tool options may change in the next release. Version 1 tool options
 will remain supported wherever possible in order to preserve
 operation of any existing scripts.

Package: libngs-c++3
Multi-Arch: same
Architecture: amd64 arm64
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Next Generation Sequencing language Bindings (C++ runtime)
 NGS is a new, domain-specific API for accessing reads, alignments and
 pileups produced from Next Generation Sequencing. The API itself is
 independent from any particular back-end implementation, and supports
 use of multiple back-ends simultaneously. It also provides a library for
 building new back-end "engines". The engine for accessing SRA data is
 contained within the sister repository ncbi-vdb.
 .
 The API is currently expressed in C++, Java and Python languages. The
 design makes it possible to maintain a high degree of similarity between
 the code in one language and code in another - especially between C++
 and Java.

Package: libngs-c++-dev
Multi-Arch: same
Architecture: amd64 arm64
Section: libdevel
Depends: ${misc:Depends},
         libngs-c++3 (= ${binary:Version})
Breaks: libncbi-vdb-dev (<< 3), libngs-sdk-dev (<< 3)
Replaces: libncbi-vdb-dev (<< 3), libngs-sdk-dev (<< 3)
Description: Next Generation Sequencing language Bindings (C++ development)
 NGS is a new, domain-specific API for accessing reads, alignments and
 pileups produced from Next Generation Sequencing. The API itself is
 independent from any particular back-end implementation, and supports
 use of multiple back-ends simultaneously. It also provides a library for
 building new back-end "engines". The engine for accessing SRA data is
 contained within the sister repository ncbi-vdb.
 .
 The API is currently expressed in C++, Java and Python languages. The
 design makes it possible to maintain a high degree of similarity between
 the code in one language and code in another - especially between C++
 and Java.
 .
 This is the development package for C++ usage.

Package: libncbi-ngs3
Multi-Arch: same
Architecture: amd64 arm64
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Next Generation Sequencing language Bindings (full runtime)
 NGS is a new, domain-specific API for accessing reads, alignments and
 pileups produced from Next Generation Sequencing. The API itself is
 independent from any particular back-end implementation, and supports
 use of multiple back-ends simultaneously. It also provides a library for
 building new back-end "engines". The engine for accessing SRA data is
 contained within the sister repository ncbi-vdb.
 .
 The API is currently expressed in C++, Java and Python languages. The
 design makes it possible to maintain a high degree of similarity between
 the code in one language and code in another - especially between C++
 and Java.

Package: libncbi-ngs-dev
Multi-Arch: same
Architecture: amd64 arm64
Section: libdevel
Depends: ${misc:Depends},
         libncbi-ngs3 (= ${binary:Version}),
         libngs-c++-dev (= ${binary:Version})
Description: Next Generation Sequencing language Bindings (C++ development)
 NGS is a new, domain-specific API for accessing reads, alignments and
 pileups produced from Next Generation Sequencing. The API itself is
 independent from any particular back-end implementation, and supports
 use of multiple back-ends simultaneously. It also provides a library for
 building new back-end "engines". The engine for accessing SRA data is
 contained within the sister repository ncbi-vdb.
 .
 The API is currently expressed in C++, Java and Python languages. The
 design makes it possible to maintain a high degree of similarity between
 the code in one language and code in another - especially between C++
 and Java.
 .
 This is the full development package.

Package: libngs-java
Multi-Arch: same
Architecture: amd64 arm64
Section: java
Depends: ${java:Depends},
         ${misc:Depends},
         libngs-jni (>= ${source:Version}),
         libngs-jni (<< ${source:Version}.1~)
Recommends: ${java:Recommends}
Suggests: libngs-java-doc
Description: Next Generation Sequencing language Bindings (Java bindings)
 NGS is a new, domain-specific API for accessing reads, alignments and
 pileups produced from Next Generation Sequencing. The API itself is
 independent from any particular back-end implementation, and supports
 use of multiple back-ends simultaneously. It also provides a library for
 building new back-end "engines". The engine for accessing SRA data is
 contained within the sister repository ncbi-vdb.
 .
 The API is currently expressed in C++, Java and Python languages. The
 design makes it possible to maintain a high degree of similarity between
 the code in one language and code in another - especially between C++
 and Java.
 .
 Java bindings.

Package: libngs-jni
Multi-Arch: same
Architecture: amd64 arm64
Section: java
Depends: ${shlibs:Depends},
         libncbi-ngs3 (= ${binary:Version}),
         ${misc:Depends}
Description: Next Generation Sequencing language Bindings (Java native binding)
 NGS is a new, domain-specific API for accessing reads, alignments and
 pileups produced from Next Generation Sequencing. The API itself is
 independent from any particular back-end implementation, and supports
 use of multiple back-ends simultaneously. It also provides a library for
 building new back-end "engines". The engine for accessing SRA data is
 contained within the sister repository ncbi-vdb.
 .
 The API is currently expressed in C++, Java and Python languages. The
 design makes it possible to maintain a high degree of similarity between
 the code in one language and code in another - especially between C++
 and Java.
 .
 Java native bindings.

Package: libngs-java-doc
Multi-Arch: foreign
Architecture: all
Section: doc
Depends: ${misc:Depends}
Description: Next Generation Sequencing language Bindings (Java documentation)
 NGS is a new, domain-specific API for accessing reads, alignments and
 pileups produced from Next Generation Sequencing. The API itself is
 independent from any particular back-end implementation, and supports
 use of multiple back-ends simultaneously. It also provides a library for
 building new back-end "engines". The engine for accessing SRA data is
 contained within the sister repository ncbi-vdb.
 .
 The API is currently expressed in C++, Java and Python languages. The
 design makes it possible to maintain a high degree of similarity between
 the code in one language and code in another - especially between C++
 and Java.
 .
 Documentation for the Java bindings.

Package: python3-ngs
Architecture: all
Section: python
Depends: ${python3:Depends},
         ${misc:Depends},
         libncbi-ngs3 (>= ${source:Version}),
         libncbi-ngs3 (<< ${source:Version}.1~)
Description: Next Generation Sequencing language Bindings (Python3 bindings)
 NGS is a new, domain-specific API for accessing reads, alignments and
 pileups produced from Next Generation Sequencing. The API itself is
 independent from any particular back-end implementation, and supports
 use of multiple back-ends simultaneously. It also provides a library for
 building new back-end "engines". The engine for accessing SRA data is
 contained within the sister repository ncbi-vdb.
 .
 The API is currently expressed in C++, Java and Python languages. The
 design makes it possible to maintain a high degree of similarity between
 the code in one language and code in another - especially between C++
 and Java.
 .
 Python3 bindings.
