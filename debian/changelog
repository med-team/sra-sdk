sra-sdk (3.1.1+dfsg-1) UNRELEASED; urgency=medium

  * New upstream version
  * Fix Python3.12 string syntax
    Closes: #1087096
  * Do not fail if clean target debian/libngs-jni.links does not exist

 -- Andreas Tille <tille@debian.org>  Thu, 12 Dec 2024 11:59:48 +0100

sra-sdk (3.0.9+dfsg-7) unstable; urgency=medium

  * Upload to unstable to join Mbed TLS 3.x.  (See #1075868.)
  * debian/salsa-ci.yml: Comment out aptly setup, no longer needed.
  * Add changelog entries for 3.0.3+dfsg-7 through -9 to placate dgit.

 -- Aaron M. Ucko <ucko@debian.org>  Thu, 17 Oct 2024 22:48:06 -0400

sra-sdk (3.0.9+dfsg-6) experimental; urgency=medium

  * Team upload
  * Adding missing #include <algorithm> to use std::transform

 -- Pierre Gruet <pgt@debian.org>  Tue, 20 Aug 2024 22:46:44 +0200

sra-sdk (3.0.9+dfsg-5) experimental; urgency=medium

  * Team upload
  * Building against HDF5/1.14.4.3

 -- Pierre Gruet <pgt@debian.org>  Tue, 20 Aug 2024 09:45:01 +0200

sra-sdk (3.0.9+dfsg-4) experimental; urgency=medium

  * Team upload
  * Removing unneeded markdown and discount from B-D

 -- Pierre Gruet <pgt@debian.org>  Fri, 12 Jul 2024 07:48:57 +0200

sra-sdk (3.0.9+dfsg-3) experimental; urgency=medium

  * debian/control: Build depend on libabsl-dev so binaries using libre2
    (sharq and relevant tests) can unconditionally link against
    -labsl_string_view for simplicitly.
  * debian/patches/support_libre2-11.patch (new): Prepare for (but don't
    require!) newer libre2 versions where re2::StringPiece has changed
    from a custom class to a typedef for absl::string_view, whose API is
    somewhat different.  (Closes: #1053411.)

 -- Aaron M. Ucko <ucko@debian.org>  Sun, 05 May 2024 22:45:27 -0400

sra-sdk (3.0.9+dfsg-2) experimental; urgency=medium

  * debian/patches/fix_arm64_build.patch (new): Fix arm64 builds by
    tweaking BitMagic to treat __aarch64__ like __arm64__.

 -- Aaron M. Ucko <ucko@debian.org>  Fri, 12 Apr 2024 19:32:51 -0400

sra-sdk (3.0.9+dfsg-1) experimental; urgency=medium

  [ Andreas Tille ]
  * New upstream version

  [ Aaron M. Ucko ]
  * debian/control:
    - Tighten libncbi-vdb-dev requirement to (>= 3.0.9+dfsg~) per
      upstream expectations; this stack has to stay in experimental as
      long as mbed TLS 3 does, calling for explicit hints.
    - Drop explicit B-D on python3-distutils, currently redundant with
      dh-python (which depends on it via python3-setuptools) and found
      unnecessary regardless in Ubuntu.  (Closes: #1065961.)
  * debian/patches/label_online_tests.patch:
    - Correctly place test/internal/vdb-diff/CMakeLists.txt's change; its
      set_tests_properties call accidentally wound up ahead of the
      corresponding add_test calls.
    - Label 55 additional tests as needing network access.
  * debian/salsa-ci.yml: Try to ensure availability of experimental VDB,
    both by setting RELEASE to experimental and by reinstating aptly, now
    using job 5576517.
  * debian/sra-toolkit.links: Drop; its only entry has dangled since
    vdb-get retired (in upstream version 3.0.1).  (Closes: #1040391.)
  * Standards-Version: 4.7.0 (routine-update)

 -- Aaron M. Ucko <ucko@debian.org>  Fri, 12 Apr 2024 18:02:49 -0400

sra-sdk (3.0.3+dfsg-9) unstable; urgency=medium

  * Team upload
  * Building against HDF5/1.14.4.3 (Closes: #1078882)

 -- Pierre Gruet <pgt@debian.org>  Tue, 20 Aug 2024 09:40:26 +0200

sra-sdk (3.0.3+dfsg-8) unstable; urgency=medium

  * Team upload.
  * Removing B-D on markdown or discount for building the doc
  * Building with python3-setuptools instead of removed python3-distutils

 -- Pierre Gruet <pgt@debian.org>  Thu, 11 Jul 2024 14:39:57 +0200

sra-sdk (3.0.3+dfsg-7) unstable; urgency=medium

  * debian/control: Build depend on libabsl-dev so binaries using libre2
    (sharq and relevant tests) can unconditionally link against
    -labsl_string_view for simplicitly.
  * debian/patches/support_libre2-11.patch (new): Prepare for (but don't
    require!) newer libre2 versions where re2::StringPiece has changed
    from a custom class to a typedef for absl::string_view, whose API is
    somewhat different.  (Closes: #1053411.)

 -- Aaron M. Ucko <ucko@debian.org>  Sun, 05 May 2024 22:15:17 -0400

sra-sdk (3.0.3+dfsg-6) unstable; urgency=high

  * debian/rules: Expand $(DEB_HOST_MULTIARCH) in libngs-java.links.in.
    (Closes: #1039621.)

 -- Aaron M. Ucko <ucko@debian.org>  Tue, 27 Jun 2023 22:14:41 -0400

sra-sdk (3.0.3+dfsg-5) unstable; urgency=medium

  * Limit libngs-java to those architectures where libs are available
    Closes: #1031853
  * DEP3 for all patches

 -- Andreas Tille <tille@debian.org>  Fri, 24 Feb 2023 11:52:27 +0100

sra-sdk (3.0.3+dfsg-4) unstable; urgency=medium

  * debian/rules: Explicitly build with signed characters (-fsigned-char).
    On arm64, where char is normally unsigned, two tests otherwise fail
    because messages reporting certain characters' numeric values formally
    differ from expectations.

 -- Aaron M. Ucko <ucko@debian.org>  Sun, 22 Jan 2023 17:04:21 -0500

sra-sdk (3.0.3+dfsg-3) unstable; urgency=medium

  * debian/copyright: Account for a second(!) version of BitMagic under
    tools/loaders/bam-loader, with copyright running through 2022.
  * debian/patches/no_sse4.2.patch (new): Don't use SSE 4.2.  It's a
    baseline violation on x86, and entirely unavailable elsewhere.
  * debian/patches/use_c_locale_for_comma (new): Stick with the C locale.
    en_US.UTF-8 isn't necessarily available, particularly on the buildds,
    and C is perfectly sufficient to ensure recognition of comma as a
    thousands separator.  (Recent versions of glibc supply a middle-ground
    C.utf8, but even that looks like overkill at the moment.)

 -- Aaron M. Ucko <ucko@debian.org>  Sun, 22 Jan 2023 13:26:16 -0500

sra-sdk (3.0.3+dfsg-2) unstable; urgency=medium

  * Upload to unstable.
  * .gitignore: Update patterns corresponding to test output.
  * debian/control:
    - Drop vestigial versioned dependency on libncbi-wvdb2.
    - Limit architecture-dependent packages to amd64 and arm64.  The only
      other architecture ncbi-vdb supports is x32, but these tools require
      a 64-bit address space.
  * debian/not-installed: Refresh.
    - Correct location of explicitly tagged lib*-static.a.
    - Drop spurious entries (with hardcoded amd64 paths, no less).
  * debian/patches/fix_ngs-c++-underlinkage.patch (new): Link libngs-c++
    against all internal dependencies.  Hold off for now on linking
    against libncbi-vdb, though anyone looking to substitute libncbi-wvdb
    would need to contrive to supply a suitable definition of
    KDBManagerMakeRead.
  * debian/patches/label_online_tests.patch: Extend as needed.  All
    existing labels confirmed to remain appropriate.
  * debian/patches/link_vdb_dynamically.patch: Extend prior to building
    tests again.  To wit, additionally cover makeinputs, from
    tools/loaders/general-loader/test. (All other tests were already OK at
    this point.)
  * debian/patches/prefer_dynamic_libraries.patch: Comment out upstream
    logic that favored static linkage.
  * debian/rules:
    - Skip rpaths, not needed even for libhdf5.
    - Explicitly enable tests, disabled by default these days.
    - Update test cleanup logic to allow for build area reuse.
  * debian/salsa-ci.yml: Retire remaining customizations.

 -- Aaron M. Ucko <ucko@debian.org>  Fri, 20 Jan 2023 17:57:36 -0500

sra-sdk (3.0.3+dfsg-1) experimental; urgency=medium

  [ Andreas Tille ]
  * Breaks+Replaces: libncbi-vdb-dev (<< 3), libngs-sdk-dev (<< 3)
    Closes: #1024155

  [ Aaron M. Ucko ]
  * New upstream version.
  * .gitignore:
    - Unhardcode architecture name in build tree (obj-*).
    - Ignore build results under ngs/ngs-python (dist, ngs.egg-info).
  * debian/{control,salsa-ci.yml}: Build against libncbi-vdb-dev 3.0.1+.
  * debian/copyright: Account for layout changes as of 3.0.1 and drop
    references to content removed upstream, both in Files-Excluded and in
    special cases.
  * debian/patches: Update for 3.0.1+; in particular, account for layout
    changes and better support for using system libraries.
  * debian/rules:
    - Correctly accommodate multiple supported Python versions.  (I'd
      insufficiently adapted ncbi-ngs's relevant logic.)
    - Account for layout changes and CMAKE_INSTALL_LIBDIR support (with
      the latter yielding multiarch paths).
    - Clean up newly generated files under ngs/ngs-python.
  * debian/{rules,sra-toolkit.install}: Move sratools and *-orig to a new
    /usr/lib/sra-toolkit directory; repoint symlinks accordingly.
    (Closes: #1025278.)
  * Standards-Version: 4.6.2 (routine-update)

 -- Aaron M. Ucko <ucko@debian.org>  Wed, 18 Jan 2023 21:42:39 -0500

sra-sdk (3.0.0+dfsg-1) experimental; urgency=medium

  [ Andreas Tille ]
  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * Build-Depends: default-jdk, flex, bison

  [ Aaron M. Ucko ]
  * debian/control:
    - Account for consolidation of ngs-sdk (albeit with some intervening
      changes, notably library restructuring); take the opportunity to add
      a -java-doc package.0
    - Let python3-ngs bypass libncbi-ngs-dev and use libncbi-ngs3 directly.
    - Drop build dependency on libvdb-sqlite-dev, now internal to sra-sdk.
  * debian/libngs-java-doc.javadoc: Enable javadoc installation.
  * debian/{libngs-java.{jlibs,poms},ngs-java.pom(.asc)}: Adopt (and adapt)
    from ngs-sdk.
  * debian/*ngs*.examples (new): Install upstream NGS examples.
  * debian/not-installed: Ignore usr/jar/*.jar (installed elsewhere, via
    jh_installlibs), usr/lib64/lib*-static.a, usr/share/examples*
    (installed elsewhere, via dh_installexamples), and
    usr/share/javadoc/ngs-doc (installed elsewhere, via jh_installjavadoc).
  * debian/patches/use_debian_packaged_re2.patch: Comment out an
    inappropriate add_dependencies call in test/sharq/CMakeLists.txt.
  * Add more patches for remaining build issues: fix_javadoc_build.patch,
    link_magic_dynamically.patch, link_vdb_dynamically.patch, and
    use_debian_packaged_compression_libs.patch.
  * debian/patches/use_soversions.patch (new): Give shared libraries
    proper versioned SONAMEs; use them from the Python bindings.
  * debian/rules:
    - Adopt (and adapt) assorted build logic from ngs-sdk.
    - Pass VDB header path via -DVDB_INCDIR rather than via general
      compilation flags.
    - Install configuration files explicitly; the CMake setup insists on
      putting them in the wrong place for us.
    - Extend bin deversioning to *+ (i.e., kar+).
  * debian/salsa-ci.yml: Use ncbi-vdb's aptly repo to get 3.x for now.

  [ Pierre Gruet ]
  * debian/control:
    - Add missing build dependencies on liblzma-dev and libsam-extract-dev.
    - Mark libngs-java as Architecture: all; loosen its dependencies on
      libncbi-ngs-dev slightly to account for possible binNMUs.
    - Introduce a libngs-jni package, on which libngs-java can and will
      depend in lieu of libncbi-ngs-dev (or patching source to support
      using libncbi-ngs3 directly).
  * debian/libngs-java-doc.lintian-overrides: Override complaints about
    javadoc-embedded jQuery per the discussion starting at
    https://lists.debian.org/debian-java/2018/06/msg00020.html.
  * debian/libngs-jni.links.in: Provide a general formula for libngs-jni's
    central symlink.
  * debian/rules: Add logic to fill in the specifics.

  [ Aaron M. Ucko ]
  * (debian/).gitignore (new): Ignore build and (offline) test results.
  * debian/control:
    - Make python3-ngs (safely!) Architecture: all too.
    - Attempt Architecture: any for sra-toolkit (modulo B-D availability).
      This setting matches architecture-dependent NGS packages'; if it
      doesn't work out, we'll want to rein them all in for simplicity.
    - Honor ${misc:Depends} for libngs-java per Lintian.
  * debian/copyright: Reflect a full inventory, including convenience
    copies of several third-party libraries for which it would be good to
    substitute existing packages at some point.
  * debian/patches/label_online_tests (new): Label tests that involve data
    retrieval as "online".
  * debian/patches/reflect_code_donations.patch (new): Clean up stray
    licenseless copyright statements per consultation with upstream (as
    quoted in the patch's header).
  * debian/patches/use_stock_magic.patch (new): Let copycat use the stock
    magic-number database in /usr/share/misc.
  * debian/rules:
    - Skip tests labeled "online" to avoid breaking on
      autobuilders, which require builds to work fully offline.
    - Refrain from shipping .gitignore files alongside examples.
    - Clean up after tests (offline ones, anyway).
  * Remove trailing whitespace in debian/changelog (routine-update)

 -- Aaron M. Ucko <ucko@debian.org>  Sun, 13 Nov 2022 22:22:28 -0500

sra-sdk (2.11.3+dfsg-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Fix watchfile to detect new versions on github
  * New upstream version 2.11.2.
  * Standards-Version: 4.6.0 (routine-update)
  * Bump versioned Build-Depends: libngs-sdk-dev (>= 2.11.2)
  * Skip test that test needs a *lot* of memory
  * Link against libzstd.  (Closes: #997280.)

  [ Aaron M. Ucko ]
  * d/p/fix_libngs_linkage.patch (new): Substitute ngs-c++ library for
    absent ngs library.
  * debian/rules: Stop renaming kget (to avoid a file conflict, #851219)
    now that it's retired altogether.
  * d/p/allow_multi_help.patch (new): Tolerate redundant --help
    declarations within tools/driver-tool.
  * d/p/run_unversioned_tools.patch (new): Look for tools by their
    unversioned names, per Debian's simplified installation arrangement.
  * debian/tests: Switch to modern encryption/decryption setup.
    - run-unit-test: Replace protected repository configuration (no longer
      supported as of ncbi-vdb 2.11.0) by --ngc [...].ngc.
    - test-data.tar.gz: Replace dbGaP-0.enc_key by a copy of upstream's
      test/prefetch/data/prj_phs710EA_test.ngc.
  * debian/watch: Look for tags rather than releases, as GitHub currently
    lists none(!) of the latter.
  * New upstream version 2.11.3.

 -- Aaron M. Ucko <ucko@debian.org>  Mon, 25 Oct 2021 18:29:10 -0400

sra-sdk (2.10.9+dfsg-2) unstable; urgency=medium

  * run-unit-test: Give up for now on {abi,fastq,illumina,sff}-load.
    All four rely deeply on retired VDB functionality that would be
    too awkward to reintroduce.
  * skip_broken_loaders.patch (new): Likewise stop building or trying to
    install anything from tools/sra-load, to avoid leading users on.

 -- Aaron M. Ucko <ucko@debian.org>  Thu, 04 Feb 2021 20:38:51 -0500

sra-sdk (2.10.9+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * d/watch:
    - file standard 4
    - Use filemangle to make sure the archive will not mixed up
      with other ncbi code with same version
  * Remove default debian/gbp.conf
  * Bump versioned Build-Depends: libngs-sdk-dev (>= 2.10.9)
  * Drop patch reenable_legacy_schema_in_sra_load.patch

 -- Andreas Tille <tille@debian.org>  Mon, 18 Jan 2021 09:23:28 +0100

sra-sdk (2.10.7+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Trim trailing whitespace.
  * Set field Upstream-Contact in debian/copyright.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove obsolete field Contact from debian/upstream/metadata (already present
    in machine-readable debian/copyright).

 -- Aaron M. Ucko <ucko@debian.org>  Tue, 02 Jun 2020 21:47:38 -0400

sra-sdk (2.10.6+dfsg-5) unstable; urgency=medium

  * debian/patches/reenable_legacy_schema_in_sra_load.patch: New patch to
    make use of ncbi-vdb's new SRAUseLegacyDefaultSchema toggle,
    unbreaking tools/sra-load (fastq-load et al.)  (Closes: #961347.)
  * debian/control: Ensure that this toggle is available at build time and
    at runtime.

 -- Aaron M. Ucko <ucko@debian.org>  Mon, 25 May 2020 22:45:07 -0400

sra-sdk (2.10.6+dfsg-4) unstable; urgency=medium

  * Account for new sratools setup.  (Closes: #961256.)
    - debian/rules: Rework deversioning logic to account for sratools and
      *-orig.* while still keeping hardcoding to a minimum; formally
      supply a hardcoded but originally random standard installation ID in
      a new /etc/ncbi/debian.kfg.
    - debian/patches/use_unversioned_tools.patch (new): Have sratools look
      for executable names without version extensions.

 -- Aaron M. Ucko <ucko@debian.org>  Fri, 22 May 2020 09:25:13 -0400

sra-sdk (2.10.6+dfsg-3) unstable; urgency=medium

  * Also exclude sra-pileup check_skiplist test per network access.

 -- Aaron M. Ucko <ucko@debian.org>  Thu, 21 May 2020 10:17:04 -0400

sra-sdk (2.10.6+dfsg-2) unstable; urgency=medium

  * drop_tests.patch: Exclude kar (needs network access); retire
    test-vdb-config exclusion with (fake)root out of the picture.
    Update description accordingly.
  * ignore_failing_test_for_the_moment_FIXME.patch: Resync.

 -- Aaron M. Ucko <ucko@debian.org>  Thu, 21 May 2020 09:03:42 -0400

sra-sdk (2.10.6+dfsg-1) unstable; urgency=medium

  [ Steffen Moeller ]
  * New upstream version
  * debhelper-compat 12
  * Standards-Version: 4.4.1

  * ncbi-vdb needs to appear as 2.10.2 in Debian with its mbedtls wrapper
    fixed to avoid a link error in this package.

  [ Michael R. Crusoe ]
  * Convert to Python3. Closes: #943231
  * Standards-Version: 4.5.0 (routine-update)

  [ Aaron M. Ucko ]
  * Correct accidental cutover to ncbi-vdb 2.10.5(+dfsg); further update to
    2.10.6.
  * Revert patch updates contingent on spurious cutover to ncbi-vdb.
  * Refresh patches for real.
    - Generally standardize on -pab format.
    - hardening.patch: Rebase C(P)FLAGS, which no longer request extensions.
    - python3: Drop makefile tweaks obsoleted by new PYTHON abstraction.
  * Help validate-names4.c find ncbi-vdb headers that are normally internal.
    To wit, build-depend on libncbi-vdb-dev (>= 2.10.6+dfsg-2~) to ensure
    these headers are installed sanely, and patch validate-names4.c
    accordingly in a new patch use_exposed_headers.patch.  In conjunction
    with taking the latest upstream, closes: #961049.
  * Specify configuration location by passing KONFIG_DIR.  (The makefile
    patch previously in use relied on [fake]root.)
  * Fix usage of default Debian build flags: Use buildflags.mk with
    appropriate _APPEND settings, and tweak hardening.patch to add
    DEB_*FLAGS to the front rather than the end (so as to keep upstream's
    preferred optimization level).
  * Preclean more fully to allow for consecutive builds.
  * List (wildcard) currently uninstalled files in debian/not-installed.
  * Correct logic for identifying all failing tests when there are any.
  * Secure URI in copyright format (routine-update)
  * Respect DEB_BUILD_OPTIONS in override_dh_auto_test target (routine-
    update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Remove trailing whitespace in debian/rules (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Add myself to uploaders.

 -- Aaron M. Ucko <ucko@debian.org>  Wed, 20 May 2020 22:15:19 -0400

sra-sdk (2.9.4+dfsg-1) unstable; urgency=medium

  * Team upload.

  * New upstream version.
  * Bumped policy to 4.3.0

 -- Steffen Moeller <moeller@debian.org>  Tue, 26 Feb 2019 15:01:51 +0100

sra-sdk (2.9.3+dfsg-1) unstable; urgency=medium

  * Team upload.

  * New upstream version.

 -- Steffen Moeller <moeller@debian.org>  Tue, 23 Oct 2018 15:12:22 +0200

sra-sdk (2.9.2+dfsg-2) unstable; urgency=medium

  * Architecture: amd64

 -- Andreas Tille <tille@debian.org>  Fri, 14 Sep 2018 08:24:15 +0200

sra-sdk (2.9.2+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Mon, 10 Sep 2018 08:25:37 +0200

sra-sdk (2.9.1-1+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Set Build-Depends: libncbi-vdb-dev (>=2.9.1) to avoid undefined symbol
    errors

 -- Liubov Chuprikova <chuprikovalv@gmail.com>  Thu, 05 Jul 2018 19:13:15 +0300

sra-sdk (2.9.1-1+dfsg-1) unstable; urgency=medium

  * Team upload

  [ Andreas Tille ]
  * New upstream version
  * debhelper 11
  * New Build-Depends: libvdb-sqlite-dev, versioned Build-Depends
    libncbi-vdb-dev (>= 2.9.0)
  * Exclude strangely failing test align-cache (needs investigation)

  [ Liubov Chuprikova ]
  * Add autopkgtest
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.1.4

 -- Liubov Chuprikova <chuprikovalv@gmail.com>  Sun, 01 Jul 2018 08:22:54 +0200

sra-sdk (2.8.2-5+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.1.3

 -- Andreas Tille <tille@debian.org>  Sun, 18 Feb 2018 15:44:10 +0100

sra-sdk (2.8.2-3+dfsg-1) unstable; urgency=medium

  [ Steffen Moeller ]
  * debian/upstream/metadata:
    - added references to registries
    - yamllint cleanliness

  [ Andreas Tille ]
  * New upstream version
  * cme fix dpkg-control
  * Standards-Version: 4.1.1
  * debhelper 10
  * renamed function to resolve name clash with gnutards to avoid
    FTBFS with glibc 2.25
    Closes: #882349
  * Drop test that should not run as root

 -- Andreas Tille <tille@debian.org>  Mon, 04 Dec 2017 08:29:02 +0100

sra-sdk (2.8.1-2+dfsg-2) unstable; urgency=medium

  * Fix install dir by using DEB_HOST_MULTIARCH instead of DEB_BUILD_GNU_TYPE
    (thanks for the patch to Graham Inggs <ginggs@debian.org>)
    Closes: #859261

 -- Andreas Tille <tille@debian.org>  Sat, 01 Apr 2017 23:06:00 +0200

sra-sdk (2.8.1-2+dfsg-1) unstable; urgency=medium

  * New upstream bugfix release

 -- Andreas Tille <tille@debian.org>  Tue, 24 Jan 2017 09:05:43 +0100

sra-sdk (2.8.1+dfsg-3) unstable; urgency=medium

  * Upstream suggested a different name to resolve #851219: vdb-get

 -- Andreas Tille <tille@debian.org>  Sat, 14 Jan 2017 14:41:57 +0100

sra-sdk (2.8.1+dfsg-2) unstable; urgency=medium

  * Deal with name space conflict with kget
    Closes: #851219

 -- Andreas Tille <tille@debian.org>  Fri, 13 Jan 2017 09:23:03 +0100

sra-sdk (2.8.1+dfsg-1) unstable; urgency=medium

  * New upstream version
    Closes: #846461
  * For whatever reason configure is checking for libfuse-dev so add it to
    Build-Depends
  * Build-Depends: libhdf5-dev, libmagic-dev, libxml2-dev

 -- Andreas Tille <tille@debian.org>  Wed, 11 Jan 2017 23:06:42 +0100

sra-sdk (2.7.0-2) unstable; urgency=medium

  * d/rules: Remove -pie to adapt to latest changes in dpkg
  * link against Debian packaged libmbedtls-dev
  * Enable build using ncbi-vdb version 2.8.0

 -- Andreas Tille <tille@debian.org>  Fri, 25 Nov 2016 19:08:25 +0100

sra-sdk (2.7.0-1) unstable; urgency=medium

  [ Andreas Tille ]
  * New upstream version
    Closes: #680436, #769050
  * d/watch points to github
  * d/copyright: no need to exclude any files any more
  * cme fix dpkg-control
  * New Build-Depends: libhdf5-dev, libngs-sdk-dev (>= 1.2.4)
  * drop library packages since these are now build by ncbi-vdb
  * use Github as homepage

  [ Steffen Moeller and Jon Ison ]
  * Introduced EDAM annotation to debian/upstream/edam

 -- Andreas Tille <tille@debian.org>  Thu, 01 Sep 2016 15:41:02 +0200

sra-sdk (2.3.5-2+dfsg-1) unstable; urgency=medium

  * New upstream version
  * d/rules: Adapt get-orig-source target to new uscan implementation
  * d/watch: use uversionmangle to get properly named download tarball

 -- Andreas Tille <tille@debian.org>  Wed, 30 Jul 2014 13:52:45 +0200

sra-sdk (2.3.4-2+dfsg-2) unstable; urgency=medium

  * delete symbols file
    Closes: #736411
  * lintian override for no-symbols-control-file

 -- Andreas Tille <tille@debian.org>  Thu, 23 Jan 2014 13:41:59 +0100

sra-sdk (2.3.4-2+dfsg-1) unstable; urgency=medium

  * New upstream version
  * debian/control:
     - Add missing Depends: sra-toolkit-libs0 to sra-toolkit-libs-dev
       Closes: #715133
     - Standards-Version: 3.9.5 (no changes needed)
  * debian/sra-toolkit-libs0.symbols: Adapted to new upstream version

 -- Andreas Tille <tille@debian.org>  Mon, 23 Dec 2013 22:27:00 +0100

sra-sdk (2.3.3-4~dfsg-1) unstable; urgency=low

  [ Andreas Tille ]
  * New upstream version
  * debian/upstream: Add citation information
  * fix debian/watch
  * debian/control:
     - cme fix dpkg-control
     - debhelper 9
     - canonical Vcs fields
  * debian/copyright: Exclude external libraries
  * debian/rules:
     - Add get-orig-source target to call enhances uscan
     - remove override because upstream seems to do the correct thing now
     - libsra-schema seems to be not built any more in this version so
       drop the special handling of it
     - Reactivate deletion of libs/krypto/*.pic.o.list files to get a
       really clean source
  * debian/doc: USAGE is not provided by upstream any more
  * debian/patches/build-with-system-libbz2-libz.patch: Fix build problem
    (thanks to Gert Wollny <gw.fossdev@gmail.com> for the patch)
  * debian/sra-toolkit.install: There are neither mod?? nor wmod?? files
    shipped in this version any more - so remove these from dh_install
  * debian/patches/hardening.patch: Propagate Debian hardening options
    into build system
  * debian/patches/hardening-format-security.patch: Fix format strings in
    snprintf() calls
  * debian/watch: mangle version string
  * debian/sra-toolkit-libs0.lintian-overrides: override
    package-name-doesnt-match-sonames

  [ Charles Plessy ]
  32dd5cd Updated symbols file.

 -- Andreas Tille <tille@debian.org>  Wed, 23 Oct 2013 10:43:20 +0200

sra-sdk (2.1.7a-1) unstable; urgency=low

  [ Charles Plessy ]
  61e22aa New upstream release (Closes: #627861).
  1b09fd5 Normalised debian/copyright file with config-edit.
  a93e7b5 Wildcard characters to match 64 or 32 according to the architecture.
  3d66d80 New upstream source location in debian/watch.
  74e5234 Mark the use of git-buildpackage by debian/gbp.conf.
  0074d68 Simplified Homepage URL in debian/control.
  5cd8039 Use a target that allows dynamic linking (debian/rules).
  9815817 Do not install empty /usr/bin/ncbi directory (debian/rules).
  fb42792 Updated debian/sra-toolkit-libs0.symbols.
  866f773 Install config.kfg and vdb-copy.kfg in /etc/ncbi.

  [ Andreas Tille ]
  9e6e831 Suggest patches for build system to build on non i386&amd64
    architectures as well as BSD and Hurd

 -- Charles Plessy <plessy@debian.org>  Tue, 10 Jan 2012 09:06:14 +0900

sra-sdk (2.0.1-1) unstable; urgency=low

  * Initial release (Closes: #610603).

 -- Charles Plessy <plessy@debian.org>  Tue, 24 May 2011 14:48:10 +0900
